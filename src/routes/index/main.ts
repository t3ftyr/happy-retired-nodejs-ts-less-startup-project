import * as express from 'express'
import { NextFunction } from 'connect';
export async function getMain(req: express.Request, res: express.Response, next: NextFunction){
  res.render('index/main', {title: 'Express'});
}